package com.commandline.taskmanager;

import com.commandline.taskmanager.model.BaseTask;
import com.commandline.taskmanager.model.CheckList;
import com.commandline.taskmanager.model.TimedTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskManager {

    private List<BaseTask> tasks = new ArrayList<>();

    public TaskManager(){

    }

    public BaseTask createSimpleTask(String title,String description){
        return new BaseTask(title,description);
    }

    public TimedTask createTimedTask(String title,String description){
        return new TimedTask(title,description);
    }

    public TimedTask createTimedTask(String title, String description, Date date){
        return new TimedTask(title,description,date);
    }

    public CheckList createCheckList(String title, String description){
        return new CheckList(title,description);
    }

    public CheckList createCheckList(String title, String description, List<String> items){
        CheckList list = new CheckList(title,description);
        for (String item:items){
            list.addItemToCheckList(item);
        }

        return list;
    }

    public void addTask(BaseTask baseTask){
        tasks.add(baseTask);
    }

    public void remove(BaseTask baseTask){
        tasks.remove(baseTask);
    }

    public void done(BaseTask baseTask){
        baseTask.done();
    }

    public void done(int index){
        tasks.get(index).done();
    }

    public <T extends BaseTask> T getTask(int index){
        return (T) tasks.get(index);
    }


    public List<BaseTask> getTasks() {
        return tasks;
    }
}
