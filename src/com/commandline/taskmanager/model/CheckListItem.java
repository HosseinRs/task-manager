package com.commandline.taskmanager.model;

public class CheckListItem {

    private String name;
    private boolean status;

    public CheckListItem(String name, boolean status){
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public boolean getStatus() {
        return status;
    }

    public void done() {
        status = true;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CheckListItem{" +
                "name='" + name + '\'' +
                ", status=" + status +
                '}';
    }
}
