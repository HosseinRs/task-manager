package com.commandline.taskmanager.model;


import java.util.ArrayList;
import java.util.List;

public class CheckList extends BaseTask {

    private List<CheckListItem> checkList = new ArrayList<>();

    public CheckList(String title, String description) {
        super(title, description);
    }

    public void addItemToCheckList(String string){
        checkList.add(new CheckListItem(string,false));
    }

    public boolean removeFromCheckList(String string){
        for (CheckListItem item : checkList){
            if (item.getName().equals(string)){
                return checkList.remove(item);
            }
        }
        return false;
    }

    public void doneChecklist(String name){
        for (CheckListItem item: checkList){
            if (item.getName().equals(name)){
                item.done();
            }
        }
    }

    public void doneChecklist(CheckListItem item){
        item.done();
    }

    @Override
    public String toString() {
        return "CheckList{" + "\n" +
                "title= " + getTitle() + "\n" +
                "description= " + getDescription() + "\n" +
                "checkList= " + checkList +
                '}';
    }

    public void addAllItemsToCheckList(List<String> items) {
        for (String item: items){
            addItemToCheckList(item);
        }
    }

    public List<CheckListItem> getCheckList() {
        return checkList;
    }
}
