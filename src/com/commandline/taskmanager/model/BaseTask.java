package com.commandline.taskmanager.model;

public class BaseTask {
    private String title;
    private String description;
    private boolean isDone = false;

    public BaseTask(String title, String description){
        this.title = title;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public void done(){
        isDone = true;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BaseTask {" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", isDone= " + isDone +
                '}';
    }
}
