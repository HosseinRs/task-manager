package com.commandline.taskmanager;

import com.commandline.taskmanager.model.BaseTask;
import com.commandline.taskmanager.model.CheckList;
import com.commandline.taskmanager.model.CheckListItem;
import com.commandline.taskmanager.model.TimedTask;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class UserInterface {


    private final static char CREATE_NEW_TASK = '2';
    private final static char DISPLAY_TASKS = '1';
    private final static char QUIT = 'q';

    private final static char ADD_ITEM = '1';
    private final static char EDIT_ITEM = '3';
    private final static char REMOVE_ITEM = '2';

    private final static int NORMAL_TASK = 1;
    private final static int TIMED_TASK = 2;
    private final static int CHECKLIST = 3;

    private final static int EDIT_TASK = 1;
    private final static int DELETE_TASK = 2;
    private final static int DONE_TASK = 3;


    private Scanner scanner;

    private TaskManager taskManager;

    public UserInterface(TaskManager taskManager) {
        scanner = new Scanner(System.in);
        this.taskManager = taskManager;
    }


    public void start() {
        printMainMenu();

        String input = scanner.next();

        char ch = input.charAt(0);

        switch (ch) {
            case DISPLAY_TASKS:
                startDisplayingTasks();
                break;

            case CREATE_NEW_TASK:
                createNewTask();
                break;
            case QUIT:
                System.exit(0);
                break;
        }

    }

    private void createNewTask() {
        printTaskOptions();
        int selection = scanner.nextInt();

        switch (selection) {
            case NORMAL_TASK:
                createNormalTask();
                break;

            case TIMED_TASK:
                createTimedTask();
                break;

            case CHECKLIST:
                createChecklist();
                break;
        }

        start();
    }

    private void createChecklist() {
        String title = getTitleFromUser();

        String description = getDescriptionFromUser();

        List<String> items = getCheckListFromUser();

        CheckList checkList = taskManager.createCheckList(title,description,items);

        taskManager.addTask(checkList);
    }

    private List<String> getCheckListFromUser() {
        System.out.println("enter 'q' if your done adding your items");
        List<String> items = new ArrayList<>();
        String item;
        while (!(item = scanner.next()).equals("q")){
            items.add(item);
        }
        return items;
    }

    private void createTimedTask() {
        String title = getTitleFromUser();

        String description = getDescriptionFromUser();

        Date date = getDateFromUser();
        TimedTask task;
        if (date == null){
            task = taskManager.createTimedTask(title,description);
            System.out.println("Entered date was not correct, You can edit your task later");
        } else {
            task = taskManager.createTimedTask(title,description,date);
        }

        taskManager.addTask(task);
    }

    private Date getDateFromUser () {
        // YYYY-MM-DD HH:mm:ss

        System.out.println("Please enter date in format YYYY-MM-DD HH:mm:ss");
        String formattedDate = scanner.next();
        String formattedTime = scanner.next();

        Date date = null;
        try {
            date = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss").parse(formattedDate + " " + formattedTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    private void createNormalTask() {
        String title = getTitleFromUser();

        String description = getDescriptionFromUser();

        BaseTask task = taskManager.createSimpleTask(title,description);

        taskManager.addTask(task);
    }

    private String getTitleFromUser(){
        System.out.println("Please enter a title for task");
        String title = scanner.next();

        return title;
    }

    private String getDescriptionFromUser(){
        System.out.println("Please enter a description for task");
        String description = scanner.next();

        return description;
    }

    private void printTaskOptions() {
        StringBuilder builder = new StringBuilder();

        builder.append("Welcome to TaskManager");
        builder.append('\n');
        builder.append('\n');

        builder.append("1. normal task");
        builder.append('\n');
        builder.append("2. timed task");
        builder.append('\n');
        builder.append("3. checklist");
        builder.append('\n');

        System.out.println(builder.toString());

    }

    private void startDisplayingTasks() {
        List<BaseTask> tasks = taskManager.getTasks();
        if (tasks.isEmpty()){
            System.out.println("Your tasks list is Empty");
            start();
        } else {
            printTasks(tasks);
            int index = scanner.nextInt();
            index--;
            BaseTask task = taskManager.getTask(index);
            startDisplayingTaskDetail(task);
        }
    }

    private void startDisplayingTaskDetail(BaseTask task) {
        printTaskMenu(task);
        int selection = scanner.nextInt();

        switch (selection) {
            case EDIT_TASK:
                editTask(task);
                break;

            case DELETE_TASK:
                taskManager.remove(task);
                break;

            case DONE_TASK:
                taskManager.done(task);
                break;
        }
        start();
    }

    private void editTask(BaseTask task) {
        if (task instanceof TimedTask){
            editTimedTask((TimedTask)task);
        } else if (task instanceof CheckList){
            editCheckList((CheckList)task);
        } else {
            editNormalTask(task);
        }
    }

    private void editCheckList(CheckList task) {
        String title = getTitleFromUser();
        if (!title.isEmpty()){
            task.setTitle(title);
        }
        String description = getDescriptionFromUser();
        if (!description.isEmpty()){
            task.setDescription(description);
        }

        handleCheckListItemsEdit(task);

    }

    private void handleCheckListItemsEdit(CheckList task) {
        printEditCheckMenu();

        String input = scanner.next();

        char selection = input.charAt(0);

        switch (selection){
            case ADD_ITEM:
                addItemToCheckList(task);
                break;

            case EDIT_ITEM:
                editItemFromCheckList(task);
                break;

            case REMOVE_ITEM:
                removeItemFromCheckList(task);
                break;
        }
    }

    private void editItemFromCheckList(CheckList task) {
        printCheckListItems(task);

        int index = scanner.nextInt();
        index--;

        System.out.println("Please Enter new title for 'done' to done this item");

        String selection = scanner.next();

        if (!selection.isEmpty()){
            if (selection.equals("done")){
                task.doneChecklist(task.getCheckList().get(index));
            } else {
                task.getCheckList().get(index).setName(selection);
            }
        }

    }

    private void removeItemFromCheckList(CheckList task) {
        printCheckListItems(task);

        int index = scanner.nextInt();
        index--;

        task.getCheckList().remove(index);
    }

    private void printCheckListItems(CheckList task) {
        StringBuilder builder = new StringBuilder();

        int index = 1;
        for (CheckListItem item : task.getCheckList()){
            builder.append(index);
            builder.append(". ");
            builder.append(item);
            builder.append('\n');
            index++;
        }
        System.out.println(builder.toString());
    }

    private void addItemToCheckList(CheckList task) {
        List<String> items = getCheckListFromUser();
        task.addAllItemsToCheckList(items);
    }

    private void printEditCheckMenu() {
        StringBuilder builder = new StringBuilder();

        builder.append("Please select your action");
        builder.append('\n');

        builder.append("1. add new Item to checkList");
        builder.append('\n');

        builder.append("2. remove item from checkList");
        builder.append('\n');

        builder.append("3. edit item in checkList");
        builder.append('\n');

        builder.append("q. press 'q' to quit");
        builder.append('\n');

        System.out.println(builder.toString());
    }

    private void editNormalTask(BaseTask task) {
        String title = getTitleFromUser();
        if (!title.isEmpty()){
            task.setTitle(title);
        }
        String description = getDescriptionFromUser();
        if (!description.isEmpty()){
            task.setDescription(description);
        }
    }

    private void editTimedTask(TimedTask task) {
        String title = getTitleFromUser();
        if (!title.isEmpty()){
            task.setTitle(title);
        }
        String description = getDescriptionFromUser();
        if (!description.isEmpty()){
            task.setDescription(description);
        }

        Date date = getDateFromUser();

        if (date!=null){
            task.setDuoDate(date);
        }
    }

    private void printTaskMenu(BaseTask task) {
        StringBuilder builder = new StringBuilder();
        builder.append("Welcome to TaskManager");
        builder.append('\n');
        builder.append('\n');

        builder.append(task.getTitle());
        builder.append('\n');

        builder.append("1. edit task");
        builder.append("\n");
        builder.append("2. remove task");
        builder.append("\n");
        builder.append("3. done task");
        builder.append('\n');

        System.out.println(builder.toString());

    }

    private void printTasks(List<BaseTask> tasks) {
        StringBuilder builder = new StringBuilder();

        builder.append("Welcome to TaskManager");
        builder.append('\n');
        builder.append('\n');

        int index = 1;
        for (BaseTask task : tasks) {
            builder.append(index);
            builder.append('.');
            builder.append(task);
            builder.append('\n');
            index++;
        }

        System.out.println(builder.toString());
    }

    private void printMainMenu() {
        StringBuilder builder = new StringBuilder();

        builder.append("Welcome to TaskManager");
        builder.append('\n');
        builder.append('\n');

        builder.append("1. display tasks");
        builder.append('\n');

        builder.append("2. add new task");
        builder.append('\n');

//        builder.append("3. edit task");
//        builder.append('\n');
//
//        builder.append("4. remove task");
//        builder.append('\n');

        builder.append(" type 'q' to quite");

        System.out.println(builder.toString());
    }
}
